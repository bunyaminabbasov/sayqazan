﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sayqazan.Game
{

    public class Game
    {
        private Random scopeRand = new Random();

        private int GetRandom(int min, int max)
        {
            return min + new Random().Next() % (max - min + 1);
        }

        private static string Enclose(int num)
        {
            if (num < 0)
            {
                return "(" + num + ")";
            }

            return num.ToString();
        }

        public string GenerateRandomEquation(GameLevel level)
        {
            int maxCount = 0;
            int maxnum=0;
            switch (level)
            {
                case GameLevel.EASY:
                    maxCount = 3;
                    maxnum = 10;
                    break;
                case GameLevel.MEDIUM:
                    maxCount = 4;
                    maxnum = 10;
                    break;
                case GameLevel.HARD:
                    maxCount = 5;
                    maxnum = 51;
                    break;
            }

            var numberCount = GetRandom(3, maxCount);
            var opCount = numberCount - 1;
            List<int> numberArray = new List<int>();
            List<int> opArray = new List<int>();
            for (var i = 0; i < numberCount; ++i)
            {
                var randNum = GetRandom(1, maxnum);
                //var invert = GetRandom(0, 1);
                //if (invert == 0)
                //    numberArray.Add(-randNum);
                //else
                //    numberArray.Add(randNum);
                numberArray.Add(randNum);
            }

            for (var i = 0; i < opCount; ++i)
            {
                opArray.Add(GetRandom(0, 2));
            }

            List<string> expression = new List<string>();
            expression.Add(numberArray[0].ToString());

            for (var i = 1; i < numberCount; ++i)
            {
                var eleddin = scopeRand.Next(0, 2);
                if (expression[expression.Count - 1] == ")")
                {
                    eleddin = 0;
                }

                if (eleddin == 1)
                {
                    expression.Insert(expression.Count - 1, "(");
                }

                switch (opArray[i - 1])
                {
                    case 0:
                        expression.Add(" + ");
                        expression.Add(Enclose(numberArray[i]));
                        break;
                    case 1:
                        expression.Add(" - ");
                        expression.Add(Enclose(numberArray[i]));
                        break;
                    case 2:
                        expression.Add(" * ");
                        expression.Add(Enclose(numberArray[i]));
                        break;
                }

                if (eleddin == 1)
                {
                    expression.Add(")");
                }
            }

            var sb = new StringBuilder();
            expression.ForEach(e => sb.Append(e));
            var res = sb.ToString();
            return res;
        }
    }
}
