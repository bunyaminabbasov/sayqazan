﻿using System;
using Microsoft.Extensions.Configuration;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types.ReplyMarkups;
using sayqazan.Util;
using sayqazan.Data;
using System.Linq;
using Telegram.Bot.Types;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Data;
using System.Text.RegularExpressions;
using sayqazan.Game;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Text;

namespace sayqazan.Bot
{
    public class SayqazanBot
    {
        private Timer scoreTimer;
        private readonly ITelegramBotClient botClient;
        private readonly Emoji emoji = new Emoji();
        private Game.Game game = new Game.Game();
        private bool isGameActive = true;
        private readonly IConfiguration _config = new ConfigurationBuilder()
                                                .AddJsonFile
                ("appsettings.json", true, true)
                                                .Build();
        private readonly GameDataContext db;
        public SayqazanBot()
        {
            db = new GameDataContext();
            botClient = new TelegramBotClient("977413313:AAHaC1G0OXmOZ4GXn1epxHGjOjIGlqtOvMY");
            botClient.OnMessage += BotClient_OnMessage;
            botClient.OnCallbackQuery += BotClient_OnCallbackQuery;
            botClient.OnInlineResultChosen += BotClient_OnInlineResultChosen;
            StartBot();

        }

        private void BotClient_OnInlineResultChosen(object sender, ChosenInlineResultEventArgs e)
        {
            Console.WriteLine("ssssss");
        }

        private void BotClient_OnInlineQuery(object sender, InlineQueryEventArgs e)
        {
            Console.WriteLine("ssssss");
        }

        private void SetUpTimerForStopGame()
        {
            DateTime current = DateTime.Now.AddSeconds(15);
            TimeSpan timeToGo = current - DateTime.Now;
            this.scoreTimer = new System.Threading.Timer(async x =>
            {
                SetUpTimerForStartGame(current);
                await this.StopGame();
            }, null, timeToGo, Timeout.InfiniteTimeSpan);
        }

        private void SetUpTimerForStartGame(DateTime current)
        {
            TimeSpan timeToGo = current.AddSeconds(15) - current;
            this.scoreTimer = new System.Threading.Timer(async x =>
            {
                SetUpTimerForStopGame();
                await this.StartGame();
            }, null, timeToGo, Timeout.InfiniteTimeSpan);
        }

        private async Task StopGame()
        {
            Console.WriteLine("Game Stopped!!!");
            isGameActive = false;
            var keyValue = new Dictionary<string, string>();
            keyValue.Add("newline", "\n");
            var contentAz = Util.Util.GetNormalContent((await db.Contents.FirstOrDefaultAsync(c => c.Id == "az_countingscores")).Value, keyValue);
            var contentRu = string.Empty; /*Util.Util.GetNormalContent((await db.Contents.FirstOrDefaultAsync(c => c.Id == "ru_countingscores")).Value, keyValue);*/
            var users = await db.Users.ToListAsync();
            foreach (var user in users)
            {
                try
                {
                    await botClient.SendTextMessageAsync(user.ChatId, user.Language == "az" || string.IsNullOrEmpty(user.Language) ? contentAz : contentRu);
                }
                catch (Exception ex)
                {

                    var hasStoped = ex.Message.Contains("Forbidden: bot was blocked by the user");
                    if (hasStoped)
                    {
                        db.Remove(user);
                        await db.SaveChangesAsync();

                    }
                }

            }
        }
        private async Task StartGame()
        {
            Console.WriteLine("Game Started!!!");

            var top20winners = await db.Users.OrderByDescending(u => u.Score).Take(20).ToListAsync();
            var keyValue = new Dictionary<string, string>();
            keyValue.Add("newline", "\n");
            var contentAz = Util.Util.GetNormalContent((await db.Contents.FirstOrDefaultAsync(c => c.Id == "az_top20")).Value, keyValue);
            var contentRu = string.Empty;/*Util.Util.GetNormalContent((await db.Contents.FirstOrDefaultAsync(c => c.Id == "ru_top20")).Value, keyValue);*/
            var sb = new StringBuilder();
            int counter = 1;
            foreach (var winner in top20winners)
            {
                sb.AppendLine(counter.ToString() + ". " + (string.IsNullOrEmpty(winner.Fullname) ? "Unknown Player" : winner.Fullname) + " - " + winner.Score.ToString());
                counter++;
            }
            var users = await db.Users.ToListAsync();
            foreach (var user in users)
            {
                try
                {
                    await botClient.SendTextMessageAsync(user.ChatId, user.Language == "az" || string.IsNullOrEmpty(user.Language) ? users.Count < 20 ? contentAz.Replace("20", users.Count.ToString()) + sb.ToString() : contentAz + sb.ToString() : contentRu + sb.ToString());
                }
                catch (Exception ex)
                {

                    var hasStoped = ex.Message.Contains("Forbidden: bot was blocked by the user");
                    if (hasStoped)
                    {
                        
                        db.Remove(user);
                        await db.SaveChangesAsync();

                    }
                }

            }
            isGameActive = true;
        }
        private async void BotClient_OnCallbackQuery(object sender, CallbackQueryEventArgs e)
        {
            try
            {
                sayqazan.Util.Callback callback = JsonConvert.DeserializeObject<sayqazan.Util.Callback>(e.CallbackQuery.Data);
                var user = await db.Users.FirstOrDefaultAsync(u => u.ChatId == e.CallbackQuery.Message.Chat.Id.ToString());
                if (user != null)
                {
                    var hasLang = !string.IsNullOrEmpty(user.Language);
                    user.Language = callback.Value;
                    if (callback.Type == "lang")
                    {
                        if (user.GameStatus == 0)
                        {
                            user.GameStatus = 1;
                        };
                        db.Update<sayqazan.Models.User>(user);
                        db.SaveChanges();
                        if (!hasLang)
                        {
                            await SendNextQuestion(user, false);
                        }
                        else
                        {
                            await LanguageUpdated(user);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.WriteLine("Callback: " + e.CallbackQuery.Data);
            }

        }
        private async void BotClient_OnMessage(object sender, MessageEventArgs e)
        {
            var text = e.Message.Text;
            if (!string.IsNullOrEmpty(text) && isGameActive)
            {
                switch (text)
                {
                    case "/start":
                        await Start(e.Message);
                        break;
                    case "/stop":
                        StopBot();
                        break;
                    case "/help":
                        await Command(e.Message.Chat.Id.ToString());
                        break;
                    case "/info":
                        await Info(e.Message.Chat.Id.ToString());
                        break;
                    case "/lang":
                        await SendLanguage(e.Message.Chat.Id.ToString());
                        break;
                    default:

                        await Answer(e.Message);
                        break;
                }
            }
            else
            {
                await SendIncorrectText(e.Message.Chat.Id.ToString());
            }
        }
        private async Task LanguageUpdated(Models.User user)
        {
            var keyValue = new Dictionary<string, string>();
            var content = (await db.Contents.FirstOrDefaultAsync(c => c.Id == user.Language + "_" + "langupdated")).Value;
            keyValue.Add("newline", "\n");
            keyValue.Add("question", user.LastQuestion);
            content = Util.Util.GetNormalContent(content, keyValue);
            try
            {

                await botClient.SendTextMessageAsync(user.ChatId, content);
            }
            catch (Exception ex)
            {
                var hasStoped = ex.Message.Contains("Forbidden: bot was blocked by the user");
                if (hasStoped)
                {
                    
                    db.Remove(user);
                    await db.SaveChangesAsync();

                }
            }
        }
        private async Task SendLastQuestion(sayqazan.Models.User user)
        {
            string content = (await db.Contents.FirstOrDefaultAsync(c => c.Id == user.Language + "_" + "notansweredquestion")).Value;
            Dictionary<string, string> keyValue = new Dictionary<string, string>();
            keyValue.Add("newline", "\n");
            string question = user.LastQuestion;
            question = question.Replace("*", "\u00D7").Replace("/", "\u00F7");
            keyValue.Add("question", question);
            content = Util.Util.GetNormalContent(content, keyValue);
            try
            {
                await botClient.SendTextMessageAsync(user.ChatId, content);
            }
            catch (Exception ex)
            {
                var hasStoped = ex.Message.Contains("Forbidden: bot was blocked by the user");
                if (hasStoped)
                {
                    
                    db.Remove(user);
                    await db.SaveChangesAsync();

                }
            }

        }
        private async Task SendNextQuestion(Models.User user, bool isCor)
        {
            Dictionary<string, string> keyValue = new Dictionary<string, string>();
            keyValue.Add("newline", "\n");
            GameLevel level = GameLevel.EASY;
            if (user.Score < 1000)
            {
                level = GameLevel.EASY;
            }
            else if (user.Score >= 1000 && user.Score < 2000)
            {
                level = GameLevel.MEDIUM;
            }
            else if (user.Score >= 2000)
            {
                level = GameLevel.HARD;
            }
            string question = game.GenerateRandomEquation(level);
            user.LastQuestion = question;
            db.Update<Models.User>(user);
            db.SaveChanges();
            question = question.Replace("*", "\u00D7").Replace("/", "\u00F7");
            keyValue.Add("question", question);
            var histories = db.Histories.Where(h => h.UserId == user.ChatId).ToList();
            string content = string.Empty;
            if (histories.Count == 0)
            {
                content = (await db.Contents.FirstOrDefaultAsync(c => c.Id == user.Language + "_" + "firstquestion")).Value;
            }
            else
            {
                if (isCor)
                {
                    content = (await db.Contents.FirstOrDefaultAsync(c => c.Id == user.Language + "_" + "answeredcorrect")).Value;
                    keyValue.Add("score", user.Score.ToString());
                }
                else
                {
                    content = (await db.Contents.FirstOrDefaultAsync(c => c.Id == user.Language + "_" + "questioniswrong")).Value;
                }

            }
            content = Util.Util.GetNormalContent(content, keyValue);
            try
            {
            await botClient.SendTextMessageAsync(user.ChatId, content);
            }
            catch (Exception ex)
            {
                var hasStoped = ex.Message.Contains("Forbidden: bot was blocked by the user");
                if (hasStoped)
                {
                    
                    db.Remove(user);
                    await db.SaveChangesAsync();

                }
            }
        }
        private async Task Answer(Message message)
        {
            var user = await db.Users.FirstOrDefaultAsync(e => e.ChatId == message.Chat.Id.ToString());
            if (user != null)
            {
                if (user.GameStatus > 0)
                {
                    int answer;
                    bool isCor = false;
                    bool isMatch = Regex.IsMatch(message.Text, "[^0-9-]+");
                    if (!isMatch)
                    {
                        var parsed = int.TryParse(Regex.Replace(message.Text, "[^0-9-]+", ""), out answer);
                        if (parsed)
                        {
                            var score = answer == int.Parse((new DataTable().Compute(user.LastQuestion, null)).ToString()) ? 10 : 0;
                            db.Histories.Add(new Models.History() { UserId = user.ChatId, UserAnswer = message.Text, Question = user.LastQuestion, IsCorrect = score > 0 ? true : false });
                            if (score != 0)
                            {
                                user.Score += score;
                                user.UpdatedAt = DateTimeOffset.Now;
                                isCor = true;
                                db.SaveChanges();
                            }
                            if (user.GameStatus == 1 && user.Score == 200)
                            {
                                await RealGame(user);
                                return;
                            }
                            await SendNextQuestion(user, isCor);

                        }
                    }
                    else
                    {
                        await SendIncorrectText(user.ChatId);
                    }
                }
            }
        }
        private async Task SendIncorrectText(string chatId = null, Models.User user = null)
        {
            string content = string.Empty;
            string lang = string.Empty;
            string _chatId = string.Empty;
            Models.User _user;
            if (user == null)
            {
                _chatId = chatId;
                _user = await db.Users.FirstOrDefaultAsync(e => e.ChatId == chatId);
                lang = _user == null || string.IsNullOrEmpty(_user.Language) ? "az" : _user.Language;
            }
            else
            {
                _user = user;
                _chatId = user.ChatId;
                lang = string.IsNullOrEmpty(user.Language) ? "az" : user.Language;
            }
            content = (await db.Contents.FirstOrDefaultAsync(c => c.Id == lang + "_" + "incorrecttext")).Value;
            var keyValue = new Dictionary<string, string>();
            keyValue.Add("newline", "\n");
            content = Util.Util.GetNormalContent(content, keyValue);
            try
            {
            await botClient.SendTextMessageAsync(_chatId, content);

            }
            catch (Exception ex)
            {
                var hasStoped = ex.Message.Contains("Forbidden: bot was blocked by the user");
                if (hasStoped)
                {
                    if (user != null)
                    {
                        
                        db.Update(_user);
                        await db.SaveChangesAsync();

                    }
                }
            }
        }
        private async Task Start(Message message)
        {
            var user = await db.Users.FirstOrDefaultAsync(e => e.ChatId == message.Chat.Id.ToString());
            if (user == null)
            {
                db.Users.Add(new Models.User()
                {
                    ChatId = message.Chat.Id.ToString(),
                    Fullname = message.Chat.FirstName + " " + message.Chat.LastName,
                    Username = message.Chat.Username,
                });
                db.SaveChanges();
                await SendLanguage(message.Chat.Id.ToString());
            }
            else { if (string.IsNullOrEmpty(user.Language)) { await SendLanguage(message.Chat.Id.ToString()); } else { await SendLastQuestion(user); } }
        }
        private async Task SendLanguage(string chatId = null, Models.User user = null)
        {
            string content = string.Empty;
            string lang = string.Empty;
            if (user == null)
            {
                var _user = await db.Users.FirstOrDefaultAsync(u => u.ChatId == chatId);
                if (_user != null)
                {
                    lang = string.IsNullOrEmpty(_user.Language) ? "az" : _user.Language;
                    content = (await db.Contents.FirstOrDefaultAsync(c => c.Id == lang + "_" + "lang")).Value;
                    try
                    {

                    await botClient.SendTextMessageAsync(chatId: chatId, content, replyMarkup: new InlineKeyboardMarkup(GetInlineKeyboard(new string[] { "az", "ru" }, "lang")));
                    }
                    catch (Exception ex)
                    {
                        var hasStoped = ex.Message.Contains("Forbidden: bot was blocked by the user");
                        if (hasStoped)
                        {
                            
                            db.Remove(user);
                            await db.SaveChangesAsync();

                        }
                    }
                }
                else
                {
                    lang = "az";
                    content = (await db.Contents.FirstOrDefaultAsync(c => c.Id == lang + "_" + "firstlystart")).Value;
                    try
                    {

                    await botClient.SendTextMessageAsync(chatId: chatId, content);
                    }
                    catch (Exception ex)
                    {
                        var hasStoped = ex.Message.Contains("Forbidden: bot was blocked by the user");
                        if (hasStoped)
                        {
                            
                            db.Remove(user);
                            await db.SaveChangesAsync();

                        }
                    }
                }
            }
            else
            {
                lang = string.IsNullOrEmpty(user.Language) ? "az" : user.Language;
                content = (await db.Contents.FirstOrDefaultAsync(c => c.Id == lang + "_" + "lang")).Value;
                try
                {

                await botClient.SendTextMessageAsync(chatId: chatId, content, replyMarkup: new InlineKeyboardMarkup(GetInlineKeyboard(new string[] { "az", "ru" }, "lang")));
                }
                catch (Exception ex)
                {

                    var hasStoped = ex.Message.Contains("Forbidden: bot was blocked by the user");
                    if (hasStoped)
                    {
                        
                        db.Remove(user);
                        await db.SaveChangesAsync();

                    }
                }
            }

        }
        private async Task Command(string chatId)
        {
            Dictionary<string, string> keyValue = new Dictionary<string, string>();
            var user = await db.Users.FirstOrDefaultAsync(e => e.ChatId == chatId);
            var lang = user == null || string.IsNullOrEmpty(user.Language) ? "az" : user.Language;
            var content = (await db.Contents.FirstOrDefaultAsync(c => c.Id == lang + "_" + "commands")).Value;
            keyValue = new System.Collections.Generic.Dictionary<string, string>();
            keyValue.Add("newline", "\n");
            content = Util.Util.GetNormalContent(content, keyValue);
            try
            {

            await botClient.SendTextMessageAsync(chatId, content);
            }
            catch (Exception ex)
            {

                var hasStoped = ex.Message.Contains("Forbidden: bot was blocked by the user");
                if (hasStoped)
                {
                    
                    db.Remove(user);
                    await db.SaveChangesAsync();

                }
            }
        }
        private async Task Info(string chatId)
        {
            Dictionary<string, string> keyValue = new Dictionary<string, string>();
            var user = await db.Users.FirstOrDefaultAsync(e => e.ChatId == chatId);
            if (user != null)
            {
                keyValue = new Dictionary<string, string>();
                keyValue.Add("newline", "\n");
                var lang = user.Language == null ? "az" : user.Language;
                var content = (await db.Contents.FirstOrDefaultAsync(c => c.Id == lang + "_" + "info")).Value;
                var corCount = db.Histories.Where(e => e.IsCorrect && e.UserId == user.ChatId).Count();
                var inCorCount = db.Histories.Where(e => !e.IsCorrect && e.UserId == user.ChatId).Count();
                keyValue.Add("score", user.Score.ToString());
                keyValue.Add("cor_answ", corCount.ToString());
                keyValue.Add("in_answ", inCorCount.ToString());
                keyValue.Add("question", user.LastQuestion);
                content = Util.Util.GetNormalContent(content, keyValue);
                try
                {

                await botClient.SendTextMessageAsync(chatId, content);
                }
                catch (Exception ex)
                {
                    var hasStoped = ex.Message.Contains("Forbidden: bot was blocked by the user");
                    if (hasStoped)
                    {
                        
                        db.Remove(user);
                        await db.SaveChangesAsync();

                    }
                }
            }
        }
        private void StartBot()
        {
            SetUpTimerForStopGame();
            botClient.StartReceiving();
            Console.Read();
        }
        private void StopBot()
        {
            botClient.StopReceiving();
        }
        private async Task RealGame(Models.User user)
        {
            try
            {

            await botClient.SendTextMessageAsync(user.ChatId, $"Demo Finished!!!!\nScore: {user.Score}");
            }
            catch (Exception ex)
            {
                var hasStoped = ex.Message.Contains("Forbidden: bot was blocked by the user");
                if (hasStoped)
                {
                    
                    db.Remove(user);
                   await db.SaveChangesAsync();
                }
            }
        }
        private InlineKeyboardButton[][] GetInlineKeyboard(string[] contents, string type)
        {
            var keyboardInline = new InlineKeyboardButton[1][];
            var keyboardButtons = new InlineKeyboardButton[contents.Length];

            if (type == "lang")
            {
                for (var i = 0; i < contents.Length; i++)
                {
                    keyboardButtons[i] = new InlineKeyboardButton
                    {
                        Text = contents[i].ToUpper() + " " + Util.Util.FlagEmoji(contents[i]),
                        CallbackData = i == 0 ? "{\"Type\":\"lang\",\"Value\":\"az\"}" : "{\"Type\":\"lang\",\"Value\":\"ru\"}",
                    };
                }
            }
            else if (type == "demo")
            {
                keyboardButtons[0] = new InlineKeyboardButton
                {
                    Text = contents[0],
                    CallbackData = "{\"Type\":\"demo\",\"Value\":\"start\"}",
                };
            }
            keyboardInline[0] = keyboardButtons;
            return keyboardInline;
        }
    }

}