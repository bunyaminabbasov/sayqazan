﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sayqazan.Util
{
    public class Util
    {
        public static string FlagEmoji(string country)
        {
            return string.Concat(country.ToUpper().Select(x => char.ConvertFromUtf32(x + 0x1F1A5)));
        }
        private static string EmojiByName(Emoji emoji, string name)
        {
            object em;
            try
            {
                em = emoji.GetType().GetProperty(name).GetValue(emoji, null);
                return em.ToString();
            }
            catch (Exception)
            {

                return null;
            }
        }
        public static DateTime GetScheduledSundayDate()
        {
            var now = DateTime.Now;
            int days = 0;
            switch (now.DayOfWeek.ToString())
            {
                case "Monday":
                    days = 6;
                    break;
                case "Tuesday":
                    days = 5;
                    break;
                case "Wednesday":
                    days = 4;
                    break;
                case "Thursday":
                    days = 3;
                    break;
                case "Friday":
                    days = 2;
                    break;
                case "Saturday":
                    days = 1;
                    break;
                case "Sunday":
                    days = now.Hour >= 23 && now.Minute >= 50 ? 7 : 0;
                    break;
            }
            var scheduledTime = now.AddDays(days).AddHours(23 - now.Hour).AddMinutes(50 - now.Minute).AddSeconds(0 - now.Second);
            return scheduledTime;
        }
        public static string GetNormalContent(string content, Dictionary<string, string> keyValue)
        {
            if (content.Contains("*$"))
            {
                var lst = content.Split("*$".ToCharArray()).ToList();
                for (int i = 0; i < lst.Count; i++)
                {
                    var em = EmojiByName(new Emoji(), lst[i]);
                    if (em != null)
                    {
                        lst[i] = em;
                    }
                }

                var sb = new StringBuilder();
                lst.ForEach(e => sb.Append(e));
                content = sb.ToString();
            }
            foreach (var key in keyValue.Keys)
            {
                content = content.Replace($"{{{key}}}", keyValue[key]);
            }
            return content;
        }
    }
}
