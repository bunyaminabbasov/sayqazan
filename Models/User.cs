﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sayqazan.Models
{
    public class User
    {
        public User()
        {
            this.CreatedAt = DateTimeOffset.Now;
            this.UpdatedAt = DateTimeOffset.Now;
            this.Score = 0;
            this.PaymentStatus = 0;
            this.GameStatus = 0;
        }
        [Key]
        public string ChatId { get; set; }
        public string Phone { get; set; }
        public string Fullname { get; set; }
        public string Username { get; set; }
        public string Language { get; set; }
        public int Score { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset UpdatedAt { get; set; }
        public string LastQuestion { get; set; }
        public int GameStatus { get; set; }
        public int PaymentStatus { get; set; }
        public List<History> Histories { get; set; }
    }
}