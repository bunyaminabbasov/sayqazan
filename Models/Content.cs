﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace sayqazan.Models
{
    public class Content
    {
        [Key]
        public string Id { get; set; }
        public string Value { get; set; }
    }
}
