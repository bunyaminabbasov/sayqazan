﻿namespace sayqazan.Models
{

    public class History
    {
        public History()
        {
            IsCorrect = false;
        }
        public int Id { get; set; }
        public string Question { get; set; }
        public string UserAnswer { get; set; }
        public User User { get; set; }
        public string UserId { get; set; }
        public bool IsCorrect { get; set; }
    }
}