﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using sayqazan.Models;


namespace sayqazan.Data
{
    public partial class GameDataContext : DbContext
    {
        private IConfiguration config = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json", true, true)
            .Build();
        public GameDataContext()
        {
        }

        public GameDataContext(DbContextOptions<GameDataContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder
                    .UseMySql(
                        "server=localhost;database=sayqazanDb;user=root;password=This.Mysql12345"
                        );
            }
        }
        public DbSet<User> Users { get; set; }
        public DbSet<History> Histories { get; set; }
        public DbSet<Content> Contents { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<History>()
                .HasOne(h => h.User)
                .WithMany(u => u.Histories)
                .HasForeignKey(h => h.UserId)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<Content>().HasData(new Content()
            {
                Id = "az_incorrecttext",
                Value = "Diqqət!{newline}Redaktə edilmiş tekst qəbul edilmir və qeydə alınır."
            });
            modelBuilder.Entity<Content>().HasData(new Content()
            {
                Id = "az_finishregister",
                Value = "Hörmətli abunəçi,{newline}Qeydiyyatı tamamlamaq üçün mobil nömrənizi daxil edin.{newline}Misal: 0551234567"
            });
            modelBuilder.Entity<Content>().HasData(new Content()
            {
                Id = "az_entersmscode",
                Value = "Hörmətli abunəçi,{newline}Qeydiyyatı tamamlamaq üçün SMS ilə aldığınız kodu bizə göndərin."
            });
            modelBuilder.Entity<Content>().HasData(new Content()
            {
                Id = "az_enterphone",
                Value = "Hörmətli abunəçi,{newline}Mobil nömrənizi daxil edin.{newline}Misal: 0551234567"
            });
            modelBuilder.Entity<Content>().HasData(new Content()
            {
                Id = "az_registerfinished",
                Value = "Hörmətli abunəçi,{newline}Qeydiyyat tamamlandı."
            });
            modelBuilder.Entity<Content>().HasData(new Content()
            {
                Id = "az_firstquestion",
                Value = "İlk sual:{newline}{question}"
            });
            modelBuilder.Entity<Content>().HasData(new Content()
            {
                Id = "az_checkcode",
                Value = "Hörmətli abunəçi,{newline}SMS ilə aldığınız kodun düzgünlüyünü yoxlayın."
            });
            modelBuilder.Entity<Content>().HasData(new Content()
            {
                Id = "az_notansweredquestion",
                Value = "Cavablandırılmamış sual:{newline} {question}"
            });
            modelBuilder.Entity<Content>().HasData(new Content()
            {
                Id = "az_commands",
                Value = "Komandalar:{newline}/start - Oyuna başlamaq{newline}/info - Statistika{newline}/status - Statistika{newline}/help - Oyun haqqında məlumat{newline}/lang-Dili dəyiş"
            });
            modelBuilder.Entity<Content>().HasData(new Content()
            {
                Id = "az_info",
                Value = "Məlumat:{newline}Düzgün cavablar: {cor_answ}{newline}Səhv cavablar: {in_answ}{newline}Status: Aktiv{newline}Xallar: {score}{newline}Cavablandırılmamış sual:{newline} {question}"
            });
            modelBuilder.Entity<Content>().HasData(new Content()
            {
                Id = "az_wakeup",
                Value = "Oyan və oyna.{newline}Cavablandırılmamış sual:{newline}{question}"
            });
            modelBuilder.Entity<Content>().HasData(new Content()
            {
                Id = "az_clickacceptbutton",
                Value = "Zəhmət olmasa Təstiq Et! düyməsin tıklayın"
            });
            modelBuilder.Entity<Content>().HasData(new Content()
            {
                Id = "az_answeredcorrect",
                Value = "Cavab düzdü. *$Wink*${newline}Sizin {score} xalınız var.{newline}Sual: {question}"

            });
            modelBuilder.Entity<Content>().HasData(new Content()
            {
                Id = "az_questioniswrong",
                Value = "Təəsüflər olsun cavab səhfdi. *$Pensive*${newline}Sual: {question}"
            });
            modelBuilder.Entity<Content>().HasData(new Content()
            {
                Id = "az_code",
                Value = "Telegram uchun yaradilan SMS kodu: {code}"
            });
            modelBuilder.Entity<Content>().HasData(new Content()
            {
                Id = "az_lang",
                Value = "Dil seçiminizi edin."
            });
            modelBuilder.Entity<Content>().HasData(new Content()
            {
                Id = "az_firstlystart",
                Value = "İlk öncə /start komandasini göndərin."
            });
            modelBuilder.Entity<Content>().HasData(new Content()
            {
                Id = "az_langupdated",
                Value = "Diliniz uğurla yeniləndi!{newline}Cavablandırılmamış sual:{newline}{question}"
            });
            modelBuilder.Entity<Content>().HasData(new Content()
            {
                Id = "az_countingscores",
                Value = "Həftəlik xalların hesablanması üçün {newline}oyuna 23:50-00:00 aralığında{newline}fasilə verilmişdir*$Full_Moon_With_Face*$"
            });
            modelBuilder.Entity<Content>().HasData(new Content()
            {
                Id = "az_top20",
                Value = "Həftənin top 20 qalibi:{newline}"
            });
            modelBuilder.Entity<Content>().HasData(new Content()
            {
                Id = "az_newweek",
                Value = "Yeni oyun həftəsində ugurlar*$Sweat_Smile*${newline}Cavablandırılmamış sual:{newline}{question}"
            });
        }
    }
}