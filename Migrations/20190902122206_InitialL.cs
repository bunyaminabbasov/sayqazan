﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace sayqazan.Migrations
{
    public partial class InitialL : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Contents",
                columns: new[] { "Id", "Value" },
                values: new object[] { "az_countingscores", "Həftəlik xalların hesablanması üçün {newline}oyuna 23:50-00:00 aralığında{newline}fasilə verilmişdir*$Full_Moon_With_Face*$" });

            migrationBuilder.InsertData(
                table: "Contents",
                columns: new[] { "Id", "Value" },
                values: new object[] { "az_top20", "Həftənin top 20 qalibi:{newline}" });

            migrationBuilder.InsertData(
                table: "Contents",
                columns: new[] { "Id", "Value" },
                values: new object[] { "az_newweek", "Yeni oyun həftəsində ugurlar*$Sweat_Smile*${newline}Cavablandırılmamış sual:{newline}{question}" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Contents",
                keyColumn: "Id",
                keyValue: "az_countingscores");

            migrationBuilder.DeleteData(
                table: "Contents",
                keyColumn: "Id",
                keyValue: "az_newweek");

            migrationBuilder.DeleteData(
                table: "Contents",
                keyColumn: "Id",
                keyValue: "az_top20");
        }
    }
}
