﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace sayqazan.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Contents",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contents", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    ChatId = table.Column<string>(nullable: false),
                    Phone = table.Column<string>(nullable: true),
                    Fullname = table.Column<string>(nullable: true),
                    Username = table.Column<string>(nullable: true),
                    Language = table.Column<string>(nullable: true),
                    Score = table.Column<int>(nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(nullable: false),
                    UpdatedAt = table.Column<DateTimeOffset>(nullable: false),
                    LastQuestion = table.Column<string>(nullable: true),
                    GameStatus = table.Column<int>(nullable: false),
                    PaymentStatus = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.ChatId);
                });

            migrationBuilder.CreateTable(
                name: "Histories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Question = table.Column<string>(nullable: true),
                    UserAnswer = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: true),
                    IsCorrect = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Histories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Histories_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "ChatId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Contents",
                columns: new[] { "Id", "Value" },
                values: new object[,]
                {
                    { "az_incorrecttext", "Diqqət!{newline}Redaktə edilmiş tekst qəbul edilmir və qeydə alınır." },
                    { "az_lang", "Dil seçiminizi edin." },
                    { "az_code", "Telegram uchun yaradilan SMS kodu: {code}" },
                    { "az_questioniswrong", "Təəsüflər olsun cavab səhfdi. *$Pensive*${newline}Sual: {question}" },
                    { "az_answeredcorrect", "Cavab düzdü. *$Wink*${newline}Sizin {score} xalınız var.{newline}Sual: {question}" },
                    { "az_clickacceptbutton", "Zəhmət olmasa Təstiq Et! düyməsin tıklayın" },
                    { "az_wakeup", "Oyan və oyna.{newline}Cavablandırılmamış sual:{newline}{question}" },
                    { "az_info", "Məlumat:{newline}Düzgün cavablar: {cor_answ}{newline}Səhv cavablar: {in_answ}{newline}Status: Aktiv{newline}Xallar: {score}{newline}Cavablandırılmamış sual:{newline} {question}" },
                    { "az_commands", "Komandalar:{newline}/start - Oyuna başlamaq{newline}/info - Statistika{newline}/status - Statistika{newline}/help - Oyun haqqında məlumat{newline}/lang-Dili dəyiş" },
                    { "az_notansweredquestion", "Cavablandırılmamış sual:{newline} {question}" },
                    { "az_checkcode", "Hörmətli abunəçi,{newline}SMS ilə aldığınız kodun düzgünlüyünü yoxlayın." },
                    { "az_firstquestion", "İlk sual:{newline}{question}" },
                    { "az_registerfinished", "Hörmətli abunəçi,{newline}Qeydiyyat tamamlandı." },
                    { "az_enterphone", "Hörmətli abunəçi,{newline}Mobil nömrənizi daxil edin.{newline}Misal: 0551234567" },
                    { "az_entersmscode", "Hörmətli abunəçi,{newline}Qeydiyyatı tamamlamaq üçün SMS ilə aldığınız kodu bizə göndərin." },
                    { "az_finishregister", "Hörmətli abunəçi,{newline}Qeydiyyatı tamamlamaq üçün mobil nömrənizi daxil edin.{newline}Misal: 0551234567" },
                    { "az_firstlystart", "İlk öncə /start komandasini göndərin." },
                    { "az_langupdated", "Diliniz uğurla yeniləndi!{newline}Cavablandırılmamış sual:{newline}{question}" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Histories_UserId",
                table: "Histories",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Contents");

            migrationBuilder.DropTable(
                name: "Histories");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
